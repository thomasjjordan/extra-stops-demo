# RELEASE_VERSION is used to determine where to deploy the app to.
# For example, if RELEASE_VERSION is '2' then the app might be deployed
# to /a/vmdt5 on the servers.
RELEASE_VERSION = '3'

# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!
