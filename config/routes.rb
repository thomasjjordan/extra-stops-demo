Rails.application.routes.draw do
  scope ENV['RAILS_RELATIVE_URL_ROOT'] || '/' do
    get ':site_id', to: 'extra_stops#extra_stops', as: 'extra_stops'
    match ':site_id/extra_stops', to: 'extra_stops#extra_stops', as: 'home', via: :all

    post ':site_id/collect_stops' => 'extra_stops#collect_stops', as: 'collect_stops'
    post ':site_id/add_stop', to: 'extra_stops#add_stop', as: 'add_stop'
    post ':site_id/update_stop', to: 'extra_stops#update_stop', as: 'update_stop'
    post ':site_id/clear_stops', to: 'extra_stops#clear_stops', as: 'clear_stops'
    post ':site_id/address_lookup', to: 'extra_stops#address_lookup', as: 'address_lookup'
    post ':site_id/send_data_to_parent', to: 'extra_stops#send_data_to_parent', as: 'send_data_to_parent'
  end
end
