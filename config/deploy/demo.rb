set :deploy_to, "/var/www/rails_apps/#{application}/dvl"
set :application, "#{application}_demo"
set :rails_env, 'demo'