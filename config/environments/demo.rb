# Based on production defaults
require Rails.root.join("config/environments/production")

Rails.application.configure do
  config.relative_url_root = "/a/r/extra_stops#{RELEASE_VERSION}_demo"
end
