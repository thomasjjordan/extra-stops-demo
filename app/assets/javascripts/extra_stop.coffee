
$ =>
  # sets the variable term to the token attached to the body object data.
  root = exports ? this
  root.term = $('body').data('term')
  root.stop_form_header = $('body').data('stop-form-header')
  root.first_stop_form_header = $('body').data('first-stop-form-header')
  root.submit_first_stop_button_text = $('body').data('submit-first-stop-button-text')
  root.submit_stop_button_text = $('body').data('submit-stop-button-text')
  root.missing_location_error = $('body').data('missing-location-error')
  root.no_location_found_error = $('body').data('no-location-found-error')
  root.post_code_error = $('body').data('post-code-error')
  root.add_stop_success = $('body').data('add-stop-success')
  root.delete_stop_success = $('body').data('delete-stop-success')
  root.update_stop_button_text = $('body').data('update-stop-button-text')
  root.update_stop_success = $('body').data('update-stop-success')
  root.update_stop_form_header = $('body').data('update-stop-form-header')
  root.confirm_delete_msg = $('body').data('confirm-delete-msg')
  # root. = $('body').data('')


  viewSetup()
  setActiveButton()
  showZipSearchInput()
  instantiateSortable()
  $('.cancel-button, #small-cancel-icon').click ->
    window.parent.CloseForm()

  $('#addStopModal').on 'hidden.bs.modal', (e) ->
    formReset()
    resetErrors()

  $('.address-tab').on 'show.bs.tab', (e) ->
    if $("#address-lookup-input-container").attr('style') == "display: none;"
      $('#submit-stop-button').attr 'disabled', false
    else
      $('#submit-stop-button').attr 'disabled', true

  $('.location-tab').on 'show.bs.tab', (e) ->
    $('#submit-stop-button').attr 'disabled', false

  $('#addStopModal').on 'show.bs.modal', (e) ->
    if anyStops()
      modalCloseButton()
      $('#form-close').show()
      $('.cancel-button').hide()
    else
      closeAppButton()
      $('#form-close').hide()
      $('.cancel-button').show()


  $('#details-modal-close').click ->
    $('.small-only').modal('hide')


  $('.address-lookup-btn').click =>
    if validateZip() then addressLookup()

  $('#collect-stops, #sm-save-icon').click ->
    collectStopData()

  $('#close-app').click ->
    window.parent.CloseForm()


  $('#zip-input').click =>
    $(this).removeClass('error-field')

  $('#sm-delete-stop, #delete-stop').click ->
    stopID = $(this).attr('data-stopId')
    deleteStop(stopID)

  $('#sm-edit-button' ).click ->
    $('#addStopModal, .small-only').modal('toggle')

  $('#edit-button').click ->
    $('#addStopModal').modal('toggle')

  $('#edit-button, #sm-edit-button').click ->
    buttonID = $(this).attr('data-stopId')
    loadStopData(buttonID)
    modalSwitchEditStop(buttonID)

  $('#new-stop-button').click ->
    resetAddressTab()
    modalSwitchAddStop()

  $('.nav-tabs').on 'click', ->
    resetErrors()

  $('body').on 'click', '#new-search',  ->
    showZipSearchInput()
    $('#display-address-results').hide()
    $( "#zip-input" ).val("")

  $('button#new-stop-button').on 'click', =>
    $('#addStopModal').modal('toggle')

  $('.address-tab').click =>
    $( "select.location-select").val("")


  $('select.location-select').change  ->
    $('select#address-results > option').empty().val("")
    $( "select.location-select").not($(this)).val("")

  $('.location-select').click =>
    resetErrors()

  $('ul#list-of-stops').on 'click', 'button.stop', (event) ->
    #Hide reset detail list item css to display none.
    $("li[data-detail]").css('display', 'none')
    buttonID = $(this).attr('data-buttonId')
    selectedData = $(this).attr('data-stop')
    $('#edit-button, #sm-edit-button').attr('data-stop', selectedData)
    $('#edit-button, #sm-edit-button, #submit-stop-button, #sm-delete-stop, #delete-stop').attr('data-stopId', buttonID)
    $('button.stop').not(this).removeClass 'active-stop'
    $(this).addClass 'active-stop'
    listStopDetails($(this))
    resetAddressTab()


  $('textarea.stop').focus ->
    $(this).animate {height: '114px'}, 500

  $(window).resize onsize
  onsize()

activateAddressTab = (stopData) ->
  if stopData.PostKey?
    emptyPostKey = stopData.PostKey.length == 0
    if !emptyPostKey
      $('.address-tab').tab('show')
      $('#lookup-results').html(
        "<h4>#{stopData.PickupLocation}</h4>" +
        "<h5>#{stopData.PickupCity},</h5>" +
        "<h5>#{stopData.PickupZip}</h5>")
      displayLookupResults()
    else console.log "No PostKey Here, Boss."

addressLookup = (zipValue) =>
  $.post $('body').data('urls')['address_lookup'], {'zip': $('#zip-input').val()},
    (data) =>
      if data.error
        $('#zip-input').val("").focus()
        $('#zip-input').addClass('error-field')
        $('#zip-input, #address-results').addClass('animated shake').one 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', =>
          $('#zip-input,#address-results ').removeClass('animated shake')
        $('.modal').animate { scrollTop: 0 }, 200
        msg = data.error
        alertError(msg).delay(3000).slideUp 200, =>
          $('.info-box-msg').remove()
      else
        showAvailableAddr()
        $('.address-lookup > :input').val("")
        $("#available-address-container").remove()
        $('#addr-well').prepend(data.addresses)
        $('#submit-stop-button').attr 'disabled', false
        $('#address-results').find('option:first-child').attr 'selected', true

#Works fine as far as I can tell.
# ***UPDATE*** All of my ajax calls need to have error handling.
addStop = ->
  formData =  $('form.extra-stops-form').serialize()
  $.post $('body').data('urls')['add_stop'],$('form.extra-stops-form').serialize(),
    (data) =>
      $('#submit-stop-button').text("#{submit_stop_button_text}")
      $('#addStopModalLabel').text("#{stop_form_header}")
      $('.stop-dashboard').show()
      $('#collect-stops, #close-app').show()
      $('#first-stop-indicator').val(false)
      $('#new-stop-button').before(data.stop)
      instantiateSortable()
      setActiveButton()
      showZipSearchInput()
      $('body').animate { scrollTop: 0 }, 200
      alertSuccess("#{add_stop_success}").delay(3000).slideUp 200, =>
        $('.info-box-msg').remove()

# Probably not the best implementaion of alerts. Definitely open to suggestions.
alertError = (msg)=>
  $('.modal-info-box').html "<div class='alert alert-danger info-box-msg' id='info-box-error' role='alert'>#{msg}</div>"
  $('.modal-info-box').fadeIn 'fast'

alertSuccess = (msg)=>
  $('.info-box').html "<div class='alert alert-success info-box-msg' id='info-box-success' role='alert'>#{msg}</div>"
  $('.info-box').fadeIn 'fast'

anyStops = ->
 $('#list-of-stops > button.stop').length > 0

cancel = =>
  window.parent.CloseForm()

viewSetup = =>
  $('#addStopModal').modal keyboard: false, backdrop: "static", show: false
  if anyStops()
    modalCloseButton()
    $('.cancel-button').hide()
    $('button[data-dismiss]').show()
    $('.stop-dashboard').show()
    $('#collect-stops, #close-app').show()
  else
    noStops()

noStops = ->
  closeAppButton()
  $('.stop-dashboard').hide()
  $('#collect-stops, #close-app').hide()
  $('.cancel-button').show()
  $('button[data-dismiss]').hide()
  formReset()
  $('#addStopModal').modal('show')
  $('#submit-stop-button').text("#{submit_first_stop_button_text}")
  $('#addStopModalLabel').text("#{first_stop_form_header}")
  $('#submit-stop-button').off().click (e) =>
    if validateForm() then addStop()

modalCloseButton = =>
  $('.close-button').off().click (e) =>
    $('#addStopModal').modal('hide')


closeAppButton = =>
  $('.close-button').off().click (e) =>
    window.parent.CloseForm()

collectStopData = =>
  stop_list =  $('button.stop')
  stops = []
  for stop in stop_list
    stopData = $(stop).data('stop')
    stopPosition = $(stop).data('buttonid')
    collectedStop = {stop: stopData, position: stopPosition}
    stops.push(collectedStop)
  $.post $('body').data('urls')['collect_stops'], {collected_stops: stops},
    (data) =>
      console.log "Data"
      console.log data
      window.parent.TransferData(data.type, data.result, data.data, data.sfx, data.opt1)
      window.parent.CloseForm()
      console.log data.type, data.result, data.data, data.sfx, data.opt1

deleteStop = (stopId) =>
  targetButton =   $("ul#list-of-stops").find("[data-buttonId='" + stopId + "']")
  $('p.alert-box').addClass('bg-success')
  if confirm "#{confirm_delete_msg}"
    $('.small-only').modal('hide')
    targetButton.fadeOut 400, =>
      targetButton.remove()
      setActiveButton()
      alertSuccess("#{delete_stop_success}").delay(3000).slideUp 200, =>
        $('.info-box-msg').remove()
        noStops() unless anyStops()

showZipSearchInput = ->
  $("#address-lookup-input-container").show()
  $("#search-link-container").hide()
  $("#available-address-container").hide()

showAvailableAddr = ->
  $('#submit-stop-button').attr 'disabled', false
  $("#address-lookup-input-container").hide()
  $("#search-link-container").show()
  $("#available-address-container").show()

displayLookupResults = ->
  $('#lookup-results').show()
  $("#display-address-results").show()
  $("#search-link-container").show()
  $("#available-address-container").hide()
  $("#address-lookup-input-container").hide()

resetAddressTab = ->
  showZipSearchInput()
  $('#display-address-results').hide()
  $('select#address-results > option').empty().val("")
  $('#lookup-results').empty()

# Seems to work fine.
formReset = =>
  $( "#zip-input" ).text("")
  $('.location-tab').tab('show')
  $('form.extra-stops-form').find('option:first-child').attr 'selected', true
  formElements = $('form.extra-stops-form').find('textarea, input[type=text], select',)
  for elm in formElements
    $(elm).val('')

indexStops = =>
  stopCount = $('.individual-stop')
  i = 1
  for stop in stopCount
    $(stop).attr('data-buttonId', i++)

instantiateSortable = =>
  $('.sortable').sortable(items: ':not(#new-stop-button)', forcePlaceholderSize: true ).bind 'sortupdate', (e, ui) ->
    indexStops()
  # $('.sortable').sortable(items: ':not(#new-stop-button)', forcePlaceholderSize: true ).bind 'sortupdate', ->
  #   indexStops()

listStopDetails = (stop) =>
  stopDetails = $(stop).data('stop')
  $('#pickup-location').show()
  stop = $(stop)
  buttonID = $(stop).attr('data-buttonId')
  $('#edit-button, #sm-edit-button, #sm-delete-stop, #delete-stop').attr('data-stopId', buttonID)
  for own key, value of stopDetails
    unless key? and not value?
      $('#pickup-location').hide() if key == 'PostKey'
      key = "PickupComments" if key == "PUDDirections"
      for field in $('.detail-element-label') when $(field).data("detail") == key
        $("li[data-detail=#{key}]").css('display', 'block')
        $(field).text(key.replace /Pickup/, "")
      for field in $('.detail-element') when $(field).data("detail") == key
        $(field).text(value)


loadStopData = (stopId) ->
  stopData = $("button.stop[data-buttonId='#{stopId}']").data('stop')
  activateAddressTab(stopData)
  for field in $('.extra-stops-form :input') when field.hasAttribute("data-param")
    $(field).val( stopData[$(field).data('param')] )
  $(".location-select option[value='#{stopData.PickupKey}']").closest('select.location-select').val("#{stopData.PickupKey}")

modalSwitchAddStop = =>
  $('#submit-stop-button, #addStopModalLabel').text("#{submit_stop_button_text}")
  $('#submit-stop-button').off().click (e) =>
    if validateForm() then addStop()

modalSwitchEditStop = (buttonID) ->
  $('#addStopModal').modal('handleUpdate')
  $('#submit-stop-button').text("#{update_stop_button_text}")
  $('#addStopModalLabel').text("#{update_stop_form_header}")
  $('#submit-stop-button').off().click (e) ->
    buttons = $( "button[data-buttonId]" )
    for button in buttons
      if $(button).attr("data-buttonId") == buttonID
        targetButton =  $(button)
    updateStop(targetButton)

onsize = =>
  test = $('#test')
  W = $(window).width()
  if W >= 768
    test.html 'Desktop'
    $('#content-window').removeClass('no-padding')
    $('#first-column').removeClass('no-padding')
    $('.small-only').attr('id', 'disabled')

  else
  if W <= 767
    test.html 'Mobile'
    $('#content-window').addClass('no-padding')
    $('#first-column').addClass('no-padding')
    $('.small-only').attr('id', 'stopDetailModal')
  return

pageReset = =>
  $('#detials-panel').show()
  $('#extra-stop-details-list').show()
  $('#add-stop-container').hide()
  $('#update-stop-container').hide()

resetErrors = =>
  for select in $('.location-select')
    $(select).removeClass('error-field')
    $('#zip-input').removeClass('error-field')

setActiveButton = ->
  lastButton = $('button.stop:last')
  $('button.stop').removeClass 'active-stop'
  $(lastButton).addClass 'active-stop'
  indexStops()
  listStopDetails(lastButton)

validateForm = =>
  addrTab = $('.address-tab').attr('id')
  locTab = $('.location-tab').attr('id')
  activeTab = $( "li.active > a" ).attr('id')
  if activeTab == locTab and validateLocation() then true and $('#addStopModal').modal('hide')
  else if activeTab == addrTab and validateZipTab() then true and $('#addStopModal').modal('hide')

validateZipTab = ->
  if $("#address-lookup-input-container").attr('style') == "display: none;"
    validateZipSelectValue()
  else
    validateZip()

validateZipSelectValue = ->
  if $('select#address-results > option').val()
    true
  else
    $('select#address-results').addClass('error-field')
    $('select#address-results').addClass('animated shake').one 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', =>
      $('select#address-results').removeClass('animated shake')
    $('.modal').animate { scrollTop: 0 }, 200
    alertError("#{missing_location_error}").delay(3000).slideUp 200, =>
      $('.info-box-msg').remove()
    false

validateZip =  =>
  zipInputValue = $.trim($('#zip-input').val())
  emptyZip = zipInputValue.length == 0
  if emptyZip
    $('#zip-input').addClass('error-field')
    $('#zip-input, #address-results').addClass('animated shake').one 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', =>
      $('#zip-input,#address-results ').removeClass('animated shake')
    $('.modal').animate { scrollTop: 0 }, 200
    alertError("#{post_code_error}").delay(3000).slideUp 200, =>
      $('.info-box-msg').remove()
    false
  else
    true

validateLocation = =>
  emptySelect =[]
  for select in $('select.location-select') when $(select).val() == ""
    emptySelect.push(select)
  emptySelect.length == $('select.location-select').length
  selectValuesEmpty = emptySelect.length == $('select.location-select').length
  if selectValuesEmpty
    $('.location-select').addClass('error-field')
    $('#loc-column').addClass('animated shake').one 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', =>
      $('#loc-column').removeClass('animated shake')
    $('.modal').animate { scrollTop: 0 }, 200
    alertError("#{missing_location_error}").delay(3000).slideUp 200, =>
      $('.info-box-msg').remove()
    false
  else
    true

updateStop = (targetButton, e) ->
  if validateForm()
    $.post $('body').data('urls')['update_stop'],$('form.extra-stops-form').serialize(),
    (data) =>
      updatedData = data.stop
      targetButton.data('stop', updatedData)
      $('button.stop').removeClass 'active-stop'
      $(targetButton).addClass 'active-stop'
      updateButtonDetails(targetButton)
      listStopDetails(targetButton)
      $('body').animate { scrollTop: 0 }, 200
      alertSuccess("#{update_stop_success}").delay(3000).slideUp 200, =>
        $('.info-box-msg').remove()

updateButtonDetails = (stop) =>
  stopDetails = $(stop).data('stop')
  $(stop).find(".list-group-item-heading").text(stopDetails.PickupLocation)
  $(stop).find(".list-group-item-text").text(stopDetails.PickupCity)


