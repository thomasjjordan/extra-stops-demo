require 'json'
class ExtraStopsController < ApplicationController
  include ExtraStopsHelper
  before_filter :precache_tokens, except: 'index'
  before_filter :precache_locations

  def add_stop
    stop_params        = params[:stop]
    @index             = stop_params[:ExtraStop].to_i + 1
    re_hash_pickup_key = stop_params[:PickupKey].values.join
    stop_params.merge!(PickupKey: re_hash_pickup_key, ExtraStop: @index)
    @stop = get_location_details(stop_params)

    stop = render_to_string(
      partial: 'stop',
      object: @stop)
    render json: { stop: stop }
  end

  def address_lookup
    @addresses = AddressLookup.for(@site_id, Address: params[:address], City: params[:city],
                                             ZipCode: params[:zip], State: params[:state], AirportCode: @airport_code)

    if @addresses.empty?
      render json: { error: 'No locations were found.' }
    else
      addresses = render_to_string(
        partial: 'address_lookup_results',
        object: @addresses)
      render json: { addresses: addresses }

    end
  end

  def clear_stops
    @passenger_details = PassengerDetails.new(@site_id, [])
    render action: 'send_data_to_parent'
  end

  def collect_stops
    stops = params[:collected_stops]
    allStops = []
    stops.each do |stop|
      stop[1][:stop][:ExtraStop] = stop[1][:position]
      allStops << stop[1][:stop]
    end
    @passenger_details = PassengerDetails.new(@site_id, allStops)
    render json: { type: 'EXTRASTOPS', result: 0, data: @passenger_details.to_csv, sfx: @sfx, opt1: fields_list }
  end

  def extra_stops
    @index = 1
    @stop  = ExtraStop.new

    @locations = Locations.by_type(@site_id,
                                   airport_code: @airport_code, group_id: @group_alias,
                                   service_area: @service_area)

    @stops = puds unless params[:user_alias].blank?

    unless params[:formdata].blank? # Incoming extra stop CSV
      incoming_stops = PassengerDetails.from_csv(@site_id, params[:formdata]).stops

      @stops = []

      incoming_stops.each do |stop|
        @stops << get_form_data_details(stop)
      end
    end

  rescue HudsonWebService::ServiceError => e
    if e.to_s =~ /Invalid airport code/
      flash[:error] = 'Missing or invalid airport code parameter.'
      redirect_to action: 'error'
      return
    else
      raise
    end
  end

  def send_data_to_parent
    stops = params[:stop].values rescue []
    add_location_details(stops)
    logger.debug("stops are #{stops.inspect}")
    @passenger_details = PassengerDetails.new(@site_id, stops)
  end

  def update_stop
    stop_params        = params[:stop]
    @index             = stop_params[:ExtraStop].to_i + 1
    re_hash_pickup_key = stop_params[:PickupKey].values.join
    stop_params.merge!(PickupKey: re_hash_pickup_key, ExtraStop: @index)
    stop = get_location_details(stop_params)
    render json: { stop: stop }
  end

  private

  def address_lookup_type
    service = token('GEN_ADDRLISTSVC')
    service = 1 if service.blank?
    service.to_i == 1 ? :map_point : :afd
  end

  def first_stop?
    @stops.nil?
  end

  helper_method :first_stop?

  def incoming_stop?
    !params[:formdata].blank? || !puds.nil?
  end

  helper_method :incoming_stop?

  def get_form_data_details(stop)
    stop = stop.data

    loc = location_search_filter(stop)

    if loc.nil?
      address = AddressLookup.for(@site_id, Address: stop[:PickupAddr], City: stop[:PickupCity],
                                            ZipCode: stop[:PickupZip], State: '', AirportCode: @airport_code)

      address_post_key = address[0].post_key
      stop.merge!(PostKey: address_post_key, PickupLocation: stop[:PickupAddr])

    else
      stop.merge!(PickupLocation: loc.Location, PickupKey: loc.Code)
    end
  end

  def location_search_filter(stop)
    Locations.for(current_site_id,
                  airport_code: @airport_code,
                  group_id: @group_alias,
                  service_area: @service_area).find do |loc|
      if stop[:PickupLocation].blank?
        loc.Address == stop[:PickupAddr]
      else
        loc.Location == stop[:PickupLocation]
      end
    end
  end


  def get_location_details(stop)
# Rails.logger.debug "\nNew Stop: Before Processing\n#{stop.pretty_inspect}\n"
    if stop.is_a? PUD

      stop = { PickupLocation: stop.Location,
               PickupCity: stop.City,
               PickupState: stop.State,
               PickupZip: stop.Zip,
               PickupKey: stop.FareCode,
               PickupAddr: stop.Address,
               PUDDirections: stop.Directions }

    elsif address_lookup_type == :afd && !stop[:PostKey].blank?

      address = AddressLookup.details(@site_id, stop[:PostKey], @airport_code)
      stop.merge!(PickupLocation: address.template,
                  PickupCity: address.city,
                  PickupState: address.state,
                  PickupZip: address.zip_code,
                  PickupKey: address.fare_key,
                  PickupAddr: address.street)

    else

      # There's currently an optimiziation issue wiht Locations.for_key.
      # This is a quick fix until the issue can be fixed.
      loc = Locations.for(current_site_id,
                          airport_code: @airport_code,
                          group_id: @group_alias,
                          service_area: @service_area).find do |loc|
        loc.Code == stop[:PickupKey]
      end

      stop.merge!(PickupLocation: loc.Location,
                  PickupCity: loc.City,
                  PickupState: loc.State,
                  PickupZip: loc.Zip,
                  PickupKey: loc.Code,
                  PickupAddr: loc.Address)

      end
  # Rails.logger.debug "\nNew Stop: After Processing\n#{stop.pretty_inspect}\n"
  stop
   end

  def precache_locations
    loc = Locations.for(current_site_id,
                        airport_code: @airport_code,
                        group_id: @group_alias,
                        service_area: @service_area)
  end

  def precache_tokens
    Tokens.for(@site_id,
               %w(EXTRASTOPS_TERM CSSFONTCOLOR CSSBGCOLOR SEC_BG SEC_TEXT SUB_BG SUB_TEXT
                  PICKUPINFO_LOCTAB1 PICKUPINFO_LOCTAB2 PICKUPINFO_LOCCAPTIONS PICKUPZIPDESC
                  EXTRASTOPS_TERM), GroupAlias: @group_alias)
    Tokens.for(@site_id, %w(EXTRASTOPS_CAPTIONS EXTRASTOPS_DBNAMES PICKUPCITYDESC PICKUPHOTELDESC
                            PICKUPAIRPORTDESC PICKUPOTHERDESC PREFPICKUPDESC))
  end

  def puds
    unless @user_alias.blank?
      incoming_puds =  Profiles.puds(@site_id, @user_alias).dup.delete_if { |p| p.Airport.downcase != @airport_code.downcase }
      pud_stops = incoming_puds.collect { |pud| get_location_details(pud) }
      return pud_stops
    end
    nil
  end

  def token(name)
    Tokens.for(@site_id, name, GroupAlias: @group_alias)
  end

  def zip_label
    label = token('PICKUPZIPDESC')
    label = 'Zip Code' if label.blank?
    label
  end
end
