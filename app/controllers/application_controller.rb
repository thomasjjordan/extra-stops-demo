class ApplicationController < ActionController::Base
  protect_from_forgery

  before_action :allow_iframe_requests
  before_filter :set_session

  def allow_iframe_requests
    response.headers.delete('X-Frame-Options')
  end

  def current_session
    session[current_site_id] ||= {}
  end

  def set_session
    session[:site_id] = params[:site_id] if params[:site_id]
    @site_id          = session[:site_id]

    if params[:group_alias].present?
      current_session[:group_alias] = params[:group_alias]
    end
    @group_alias = current_session[:group_alias]

    if params[:service_area].present?
      current_session[:service_area] = params[:service_area]
    end
    @service_area = current_session[:service_area]

    if params[:user_alias].present?
      current_session[:user_alias] = params[:user_alias]
    end
    @user_alias                    = current_session[:user_alias]

    current_session[:close_dialog] = params[:close] if params[:close].present?
    @close_dialog                  = current_session[:close_dialog]

    current_session[:sfx]          = params[:sfx] if params.key?(:sfx)
    @sfx                           = current_session[:sfx]

    if params[:airport_code].present?
      current_session[:airport_code] = params[:airport_code]
    end
    @airport_code = current_session[:airport_code]

    if params.key?(:show_addrlu)
      current_session[:show_addrlu] = params[:show_addrlu]
    end
    @show_addrlu = current_session[:show_addrlu]
  end

  private
  def current_group_alias
    session[@site_id][:group_alias] = params[:group_alias] if params[:group_alias]
    session[@site_id][:group_alias]
  end

  alias :current_group_id :current_group_alias
  helper_method :current_group_alias, :current_group_id

  def current_site_id
    session[:site_id] = params[:site_id] if params[:site_id]
    @site_id          = session[:site_id]
  end

  helper_method :current_site_id

  def send_exception_report(exception)
    unless Rails.env.development?
      ExceptionNotifier::Notifier.exception_notification(request.env, exception).deliver
    end
  end
end
