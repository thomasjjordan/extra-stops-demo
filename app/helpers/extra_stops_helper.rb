module ExtraStopsHelper
  def address_lookup
    @content = content_tag(:div, class: 'form-group address-lookup') do
      concat content_tag(:label, 'Post Code')
      concat text_field_tag('zip', nil, name: 'stop[zip]',
                                        id: 'zip-input', class: 'zip-search stop form-control', data: { param: 'zip' })
      concat content_tag(:button, 'Find Address', type: 'button', class: 'address-lookup-btn btn btn-primary pull-left',
                                                  id: 'zip', data: { loading_text: 'Searching...' })
    end
    @content << content_tag(:div, class: 'form-group address-results') do
      concat content_tag(:label, 'Available Addresses', class: 'address-results')
      concat select_tag('address-results', '', name: 'stop[PostKey]', class: 'stop form-control address-select',
                                               id: 'address-results')
      concat link_to('Lookup Different Address', '#', class: 'new-search address-results', id: 'new-search')
    end
  end

  #               <%#= select_tag("address_results_#{form_id}",
  #                              available_addresses(@addresses),
  #                              :class => 'stop form-control location-select address_results_list',
  #                              :id => "address_results_select_#{form_id}",
  #                              :onchange => "setPostKey(#{form_id},
  #                                           $('address_results_select_'+#{form_id}).getValue());") %>

  def location_select_builder(locations)
    locations.map do |type|
      location_array = []
      location_type  = type[:label]
      type[:locations].each do |loc|
        location_array << [loc.Name, loc.Code]
      end
      content_tag(:div, class: 'form-group') do
        concat content_tag(:label, location_type.capitalize.html_safe, class: 'stop-label')
        concat select_tag "#{location_type.downcase}", options_for_select(location_array), include_blank: true,
                                                                                           name:  "[stop][PickupKey][loc_type_#{type[:type]}]", class: 'stop form-control location-select',
                                                                                           id:    "#{type[:type]}"
      end
    end.join("\n").html_safe
  end

  def field_builder(fields)
    fields.map do |field|
      next if skip_field?(field)
      content_tag(:div, class: 'form-group') do
        concat content_tag(:label, "#{field.caption}", class: 'stop-label')
        case field.field
        when /Passengers/
          concat select_tag('[stop][Passengers]',
                            options_for_select(0..59), include_blank: true ,
                                                       name: "[stop][#{field.field.capitalize}]", class: 'stop-info stop form-control', data: { param: 'Passengers' },
                                                       id: "#{field.caption.downcase}")
        when /PUDDirections/
          concat text_area_tag('[stop][PUDDirections]', nil, name: '[stop][PUDDirections]',
                                                             size: '30x5',
                                                             id: "#{field.caption.downcase}", class: 'stop-info stop form-control expand', data: { param: 'PUDDirections' }, style: 'margin-left:0')
        else
          concat text_field_tag("#{field.field.capitalize}", nil, name: "[stop][#{field.field.capitalize}]",
                                                                  id: "#{field.caption.downcase}", class: 'stop-info stop form-control', data: { param: "#{field.field.capitalize}" })
        end
      end
    end.join("\n").html_safe
  end

  def new_stop_button_content
    content = content_tag(:i, nil, class: ['fa', 'fa-plus-circle', 'fa-3x', 'pull-left'])
    content << content_tag(:h4, 'ADD ADDITIONAL STOP', class: 'list-group-item-heading')
    content << content_tag(:p, 'CLICK TO ADD MORE STOPS', class: 'list-group-item-text')
  end

  def stop_info(new_stop)
    content = content_tag(:div, nil, class: ['col-lg-2', 'no-padding', 'clearfix'])
    content << content_tag(:i, nil, class: ['fa', 'fa-plus-circle', 'fa-3x', 'pull-left'])
    content << content_tag(:h4, new_stop[:PickupLocation], class: 'list-group-item-heading')
    content << content_tag(:p, new_stop[:PickupAddr], class: 'list-group-item-text')
  end

  def address_label
    label = token('ROADDRESSDESC')
    label = 'Address' if label.blank?
    label.html_safe
  end

  def address_lookup_type
    service = token('GEN_ADDRLISTSVC')
    service = 1 if service.blank?
    service.to_i == 1 ? :map_point : :afd
  end

  def address_tab_label
    label = token('PICKUPINFO_LOCTAB2')
    label = 'Address' if label.blank?
    label.html_safe
  end

  # Attempts to return an appropriate form field given an extra stop field name.
  # For example, if given Passengers it would be correct to return a select list
  # whereas Name should be a text field. The default return value is a text field.
  def appropriate_field(field, stop, index)
    case field.field
    when /Passengers/
      select_tag('stop[' + index.to_s + '][' + field.field + ']',
                 options_for_select(0..59, stop.send(field.field).to_i))
    when /PUDDirections/
      text_area_tag('stop[' + index.to_s + '][' + field.field + ']',
                    stop.send(field.field), size: '30x5',
                                            class: 'text-area', style: 'margin-left:0')
    else
      text_field_tag('stop[' + index.to_s + '][' + field.field + ']',
                     stop.send(field.field), size: 30, class: 'text')
    end
  end

  def available_addresses(address_list)
    options = [['-- Select an address --', '']]

    if address_lookup_type == :afd
      address_list.each { |addr| options << [addr.template, addr.post_key] }
    else
      address_list.each do |addr|
        options << [
          "#{addr.street}, #{addr.city}, #{addr.state} #{addr.zip_code}",
          CGI.escape(Marshal.dump(addr))
        ]
      end
    end
    options_for_select(options)
  end

  def city_label
    label = token('CITYDESC')
    label = 'City' if label.blank?
    label.html_safe
  end

  def close_dialog?
    !@close_dialog.to_i.zero?
  end

  def display_address?
    displayed_address_fields.include? 'Address'
  end

  def display_city?
    displayed_address_fields.include? 'City'
  end

  def display_state?
    displayed_address_fields.include? 'State'
  end

  def display_zip?
    displayed_address_fields.include? 'ZipCode'
  end

  def displayed_address_fields
    fields = token('PICKUPINFO_LOCCAPTIONS').split(',')
    if fields.empty?
      fields = address_lookup_type == :afd ? %w(ZipCode) : %w(Address City State ZipCode)
    end
    fields
  end

  def display_address_lookup_tab?
    if @service_area.blank? && !@show_addrlu.nil?
      return @show_addrlu.to_i == 1

    else
      enabled       = token('MAPLOOKUP_ENABLED').to_i == 1
      service_areas = token('MAPLOOKUP_SERVICEAREAS').split(',').collect(&:to_i)

      enabled && service_areas.include?(@service_area.to_i)
    end
  end

  def fields
    ExtraStop.fields(@site_id)
  end

  def fields_list
    fields.collect(&:field).join(',')
  end

  def landmarks_tab_label
    label = token('PICKUPINFO_LOCTAB1')
    label = 'Landmarks' if label.blank?
    label.html_safe
  end

  def pud_label
    label = token('PREFPICKUPDESC')
    label = 'Preferred Locations' if label.blank?
    label.html_safe
  end

  def skip_field?(field)
    ['PickupAddr', 'PickupCity', 'PickupZip', 'PickupKey', 'PickupState', '', 'ExtraStop'].include?(field.field) ||
      field.caption.blank?
  end

  def state_label
    label = token('STATEDESC')
    label = 'State' if label.blank?
    label.html_safe
  end

  def zip_label
    label = token('PICKUPZIPDESC')
    label = 'Zip Code' if label.blank?
    label.html_safe
  end

  private

  def token(name)
    Tokens.for(@site_id, name, GroupAlias: @group_alias)
  end
end
