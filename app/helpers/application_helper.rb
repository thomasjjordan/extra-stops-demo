module ApplicationHelper
  def current_theme
    @theme ||= Layout::Colors.new(current_site_id)
  end

  def page_title
    content_tag :title, "Extra #{term.pluralize}"
  end

  # The word used for "Stop" (ex. in UK it's 'via')
  def term
    configured_term = Tokens.for(@site_id, 'EXTRASTOPS_TERM')
    configured_term.blank? ? 'Stop' : configured_term
  end

  def stop_form_header
    label = token("STOPFORMHEADER")
    label = "Add New #{term.capitalize}" if label.blank?
    label.html_safe
  end

  def first_stop_form_header
    label = token("FIRSTSTOPFORMHEADER")
    label = "Create First #{term.capitalize}" if label.blank?
    label.html_safe
  end

  def update_stop_form_header
    label = token("UPDATESTOPFORMHEADER")
    label = "Update #{term.capitalize}" if label.blank?
    label.html_safe
  end

  def submit_first_stop_button_text
    label = token("SUBMITSTOPBUTTONCAP")
    label = "Create First #{term.capitalize}" if label.blank?
    label.html_safe
  end

  def submit_stop_button_text
    label = token("SUBMITSTOPBUTTONCAP")
    label = "Add #{term.capitalize}" if label.blank?
    label.html_safe
  end

  def update_stop_button_text
    label = token("UPDATESTOPBUTTONCAP")
    label = "Update #{term.capitalize}" if label.blank?
    label.html_safe
  end

  def close_button_text
    label = token("CLOSEBUTTONCAP")
    label = "Cancel" if label.blank?
    label.html_safe
  end

  def confirm_delete_msg
    label = token("CONFIRMDELETEMSG")
    label = "Are You Sure You Want To Delete This #{term.capitalize}?" if label.blank?
    label.html_safe
  end

  def post_code_error
    label = token("POSTCODEERRORMSG")
    label = "Create First #{term.capitalize}" if label.blank?
    label.html_safe
  end

  def missing_location_error
    label = token("MISSINGLOCERRORMSG")
    label = "Please Select A Location." if label.blank?
    label.html_safe
  end

  def no_location_found_error
    label = token("NOLOCERRORMSG")
    label = "No Locations Found." if label.blank?
    label.html_safe
  end

  def post_code_error
    label = token("POSTCODEERRORMSG")
    label = "Please Enter A Valid #{post_code_label}." if label.blank?
    label.html_safe
  end

  def add_stop_success
    label = token("ADDEDSTOPMSG")
    label = "#{term.capitalize} Was Successfully Added." if label.blank?
    label.html_safe
  end

  def delete_stop_success
    label = token("DELETEDSTOPMSG")
    label = "#{term.capitalize} Was Successfully Deleted." if label.blank?
    label.html_safe
  end

  def update_stop_success
    label = token("UPDATEDSTOPMSG")
    label = "#{term.capitalize} Was Successfully Updated." if label.blank?
    label.html_safe
  end

  def fields
    ExtraStop.fields(@site_id)
  end

  # -- old below

  def primary_background_color
    Tokens.for(@site_id, 'S0ROWCOLOR', :GroupAlias => @group_alias)
  end

  def primary_text_color
    Tokens.for(@site_id, 'S0ROWTEXTCOLOR', :GroupAlias => @group_alias)
  end

  def secondary_background_color
    Tokens.for(@site_id, 'SEC_BG', :GroupAlias => @group_alias)
  end

  def secondary_text_color
    Tokens.for(@site_id, 'SEC_TEXT', :GroupAlias => @group_alias)
  end

  def site_background_color
    bg = Tokens.for(@site_id, 'CSSBGCOLOR', :GroupAlias => @group_alias)
    bg = 'white' if bg.blank?
    bg
  end

  def site_foreground_color
    Tokens.for(@site_id, 'CSSFONTCOLOR', :GroupAlias => @group_alias)
  end

  def site_text_color
    Tokens.for(@site_id, 'CSSFONTCOLOR', :GroupAlias => @group_alias)
  end

  def subsection_background_color
    Tokens.for(@site_id, 'SUB_BG', :GroupAlias => @group_alias)
  end

  def subsection_text_color
    Tokens.for(@site_id, 'SUB_TEXT', :GroupAlias => @group_alias)
  end

  def css_defaults
    Tokens.for(@site_id, 'CSS_CUSTOMFILE', :GroupAlias => @group_alias)
  end

  def section_header_color
    color = Paleta::Color.new(:hex, secondary_background_color.gsub(/#/, ''))
    "#" + "#{color.lighten!(10).hex}"
  end

  def active_stop_text_color
    color = Paleta::Color.new(:hex, secondary_background_color.gsub(/#/, ''))
    "#" + "#{color.lighten!(30).hex}"
  end

  def stop_button_color
    color = Paleta::Color.new(:hex, primary_background_color.gsub(/#/, ''))
    "#" + "#{color.lighten!(20).hex}"
  end
end
